import AppContainer from '../hoc/AppContainer';
import { Link } from 'react-router-dom';
import { useAuth0 } from '@auth0/auth0-react';

const Header = () => {
    const { isAuthenticated } = useAuth0();

    return (
        <AppContainer>
            <div className='mt-3 d-flex justify-content-between align-items-center'>
                <h1>Translate App</h1>
                {isAuthenticated && (
                    <nav>
                        <Link className='m-2' to='/profile'>
                            Profile
                        </Link>
                        <Link className='m-2' to='/translator'>
                            Translator
                        </Link>
                    </nav>
                )}
            </div>
        </AppContainer>
    );
};

export default Header;
