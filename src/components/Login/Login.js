import LoginButton from './LoginButton';
import { useAuth0 } from '@auth0/auth0-react';
import { Redirect } from 'react-router';
import AppContainer from '../hoc/AppContainer';

const Login = () => {
    const { isAuthenticated } = useAuth0();

    return (
        <>
            {isAuthenticated && <Redirect to='/translator' />}
            <AppContainer>
                <div>
                    <h3>Welcome to the signlanguage translator</h3>
                    <h5>Log in to get started</h5>
                    <LoginButton />
                </div>
            </AppContainer>
        </>
    );
};

export default Login;
