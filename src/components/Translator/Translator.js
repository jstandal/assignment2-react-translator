import React from 'react';
import { useState } from 'react';
import styles from './Translator.module.css';
import AppContainer from '../hoc/AppContainer';
import LogoutButton from '../Login/LogoutButton';
import TranslatorDisplay from './TranslatorDisplay';

const Translator = () => {
    const [translation, setTranslation] = useState('');
    const [translationOutput, setTranslationOutput] = useState('');
    const [history, setHistory] = useState([]);

    const onInputChange = (event) => {
        setTranslation(event.target.value);
    };

    const onTranslationSubmit = (event) => {
        setTranslationOutput(translation);
        setHistory([...history, translation]);
        event.preventDefault();
    };

    return (
        <AppContainer>
            <div className='mt-3'>
                <h3>Start translating</h3>
                <form onSubmit={onTranslationSubmit} className='input-group'>
                    <input
                        type='text'
                        className='form-control'
                        onChange={onInputChange}
                        maxLength='40'
                    />
                    <span className='input-group-btn'>
                        <button className='btn btn-success btn-lg'>
                            Translate
                        </button>
                    </span>
                </form>
                <div className={styles.TranslationContainer}>
                    {translationOutput.split('').map((translated, index) => {
                        return (
                            <TranslatorDisplay
                                translated={translated}
                                key={index}
                            />
                        );
                    })}
                </div>
                <LogoutButton />
            </div>
        </AppContainer>
    );
};

export default Translator;
