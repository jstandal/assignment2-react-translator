import styles from './Translator.module.css';

const TranslatorDisplay = ({ translated }) => {
    return (
        <figure className={styles.TranslationImage}>
            <img
                src={`${process.env.PUBLIC_URL}/assets/${translated}.png`}
                alt={translated}
            />
        </figure>
    );
};

export default TranslatorDisplay;
