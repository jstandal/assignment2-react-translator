import { applyMiddleware } from 'redux';
import { sessionMiddleware } from './sessionMiddleware';

export default applyMiddleware(sessionMiddleware);
