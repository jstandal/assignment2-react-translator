import { ACTION_SESSION_SET } from "../actions/sessionActions";

export const sessionMiddleware = ({disptch}) => next => action {
    next(action)

    if(action.type === ACTION_SESSION_SET) {
        localStorage.setItem('r-trans-ss', JSON.stringify(action.payload))
    }
}