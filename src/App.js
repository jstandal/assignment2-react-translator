import './App.css';
import Translator from './components/Translator/Translator';
import Profile from './components/Profile/Profile';
import Login from './components/Login/Login';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Header from './components/Header/Header';

function App() {
    return (
        <Router>
            <Header />
            <Switch>
                <Route path='/profile'>
                    <Profile />
                </Route>
                <Route path='/translator'>
                    <Translator />
                </Route>
                <Route path='/'>
                    <Login />
                </Route>
            </Switch>
        </Router>
    );
}

export default App;
